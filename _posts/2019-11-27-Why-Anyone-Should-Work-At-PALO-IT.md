---
layout: post
title:  "Why Anyone Should Work At PALO IT"
date:   2019-11-27
author: Sivaraam
categories: general
tags: anything
---

I am not sure about everyone else, but my journey towards Palo began with a sort of a doubt.
Yeah, sure, everybody says nice things about the company but is it really true?
From experience, most of the time, if it is too good to be true, it is too good to be true!
To my surprise, I was accepted despite my half negativity and resistance to working at a consultancy.
I kept all expectations to a flat zero and started work.

People were generally too nice and had an infectious friendliness and spontaneity that deserved reciprocity.
Often I felt like I was the grouch and the also felt negative when being exposed to something.I learnt that
whatever they pushed to us, it was to bring out the best and not the worse and to get people to mingle well.

I still have some reservation as it is still early days and I still have a long way to go before I feel that I am usefully providing the productivity for which I was hired for.Although not what I expected and do not really enjoy what I have
been provided as my duties, there is still something to learn and something to grow into.

Generally, anyone should feel comfortable to work at Palo IT as the conditions are more than friendly and approachable.There are many bright and talented individuals who are present to answer questions and provide solutions.
