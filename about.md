---
layout: page
title: About
permalink: /about/
main_nav: true
---

![alt text]({{ site.baseurl }}/assets/profile-placeholder.gif "Profile Picture"){:.profile}

I am Sivaraam, a Singaporean Indian, born and bred here.I have been educated in Singapore and lived here all my life.In my free time, I like to watch sports, TV dramas, movies and cook.
I am excited by all manner of Technology and I am currently trying to work as a DevOPs Engineer in a
local company.I have a passion towards Web Technology, trying to pick up skills in DevOPs automation, configuration and deployment.
I love to play Field Hockey and Football as well !



Centrarium is a custom theme for Jekyll, made by [Ben Centra][bencentra] for his own blog.
You can find out more info about customizing your Jekyll theme,
as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/).
And you can find the source code for Jekyll at [github.com/jekyll/jekyll](https://github.com/jekyll/jekyll)

[centrarium]: https://github.com/bencentra/centrarium
[bencentra]: http://bencentra.com
[jekyll]: https://github.com/jekyll/jekyll
Installation and configuration instructions can be found in the [GitHub repository](https://github.com/bencentra/centrarium).
